import pytest

from vega import add

PARAMETERS_ADD = [(pytest.param(1, 1, 2, id="one plus one"))]


@pytest.mark.parametrize("value1, value2, expected", PARAMETERS_ADD)
def test_add(value1, value2, expected):
    assert add(value1, value2) == expected
